# Ruby and MySQL for Bitbucket Pipelines

This repository contains a Dockerfile as well as a simple example that shows how you can run your own Docker container with Ruby and MySQL on Bitbucket Pipelines.

The Docker image is using Ruby 2.3.0 and MySQL 5.5

## Quickstart

### Using the image with Bitbucket Pipelines

Just copy/paste the YML below in your bitbucket-pipelines.yml and adapt the script to your needs.

```yaml
# This is a sample build configuration for Ruby.
# Only use spaces to indent your .yml configuration.
# -----
# You can specify a custom docker image from Dockerhub as your build environment.
image: spittet/ruby-mysql

pipelines:
  default:
    - step:
        script: # Modify the commands below to build your repository.
          - /etc/init.d/mysql start
          - mysql -uroot -e 'create database ruby'
          - gem install 'mysql2'
          - ruby test.rb
```

### Using this in a script

You'll find a sample script in this repository in test.rb. It simply connects to MySQL and then lists the existing databases.

```ruby
require 'mysql2'
con = Mysql2::Client.new(:host => "localhost", :username => "root")
dbs = con.query('SHOW DATABASES;')
dbs.each do |db|
  puts db
end

con.close
```

## Create your own image

If you want to use a different version of Ruby you can simply create your own image for it. Just copy the content of the Dockerfile and replace the first line.

This image is built from the official Ruby image at https://hub.docker.com/_/ruby/ and you can find there all the different versions that are supported.

Your Dockerfile won't need to have an ENTRYPOINT or CMD line as Bitbucket Pipelines will run the script commands that you put in your bitbucket-pipelines.yml file instead.

```
FROM ruby:2.3.0 
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update \
  && apt-get install -y mysql-server mysql-client libmysqlclient-dev --no-install-recommends \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# This Dockerfile doesn't need to have an entrypoint and a command
# as Bitbucket Pipelines will overwrite it with a bash script.
```

### Build the image

```bash
docker build -t <your-docker-account>/ruby-mysql .
```

### Run the image locally with bash to make some tests

```bash
docker run -i -t <your-docker-account>/ruby-mysql /bin/bash
```

### Push the image back to the Docker Hub

```bash
docker push <your-docker-account>/ruby-mysql
```
